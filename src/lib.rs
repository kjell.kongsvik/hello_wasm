#[no_mangle]
pub extern fn add_one(a: u32) -> u32 {
    a + 1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add_one() {
        assert_eq!(2, add_one(1));
    }
}
